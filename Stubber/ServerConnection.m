//
// Created by jelle on 07-02-14.
//
// 


#import "ServerConnection.h"
#import "AFHTTPSessionManager.h"
#import "AFHTTPRequestOperationManager.h"


@interface ServerConnection ()
@property(nonatomic, strong) AFHTTPSessionManager* httpSession;
@property(nonatomic, strong) AFHTTPRequestOperationManager* httpRequestOperationManager;
@property(nonatomic, copy) NSString* serverUrl;
@end

@implementation ServerConnection


- (id)initWithUrl:(NSString *)urlString {
    self = [super init];
    if (self) {
        self.serverUrl = urlString;
    }
    return self;
}

- (AFHTTPRequestOperationManager*)httpRequestOperationManager {
    if (_httpRequestOperationManager==nil) {
        NSURL* baseUrl = [NSURL URLWithString:self.serverUrl];
        _httpRequestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseUrl];        
    }
    return _httpRequestOperationManager;
}

- (AFHTTPSessionManager*)httpSession {
    if (_httpSession==nil) {
        NSURL* baseUrl = [NSURL URLWithString:self.serverUrl];
        _httpSession = [[AFHTTPSessionManager alloc] initWithBaseURL:baseUrl sessionConfiguration:nil];
    }
    return _httpSession;
}


- (void)nonSessionLogin:(NSString*)username password:(NSString*)password success:(SuccessBlock)success failure:(FailureBlock)failure {
    NSDictionary* parameters = @{@"user":username, @"password":password};
    [self.httpRequestOperationManager POST:@"/admin/login"
                                parameters:parameters
                                   success:^(AFHTTPRequestOperation* operation, id responseObject) {
                                       success();
                                   }
                                   failure:^(AFHTTPRequestOperation* operation, NSError* error) {
                                       if (failure) failure(error);
                                   }
    ];
}
- (void)sessionLogin:(NSString*)username password:(NSString*)password success:(SuccessBlock)success failure:(FailureBlock)failure {
    NSDictionary* parameters = @{@"user":username, @"password":password};
    [self.httpSession POST:@"/admin/login"
                parameters:parameters
                   success:^(NSURLSessionDataTask* task, id responseObject) {
                        success();
                    }
                   failure:^(NSURLSessionDataTask* task, NSError* error) {
                        if (failure) failure(error);
                    }
    ];
}


@end