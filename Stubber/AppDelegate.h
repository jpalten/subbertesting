//
//  AppDelegate.h
//  Stubber
//
//  Created by Jelle Alten on 07-02-14.
//  Copyright (c) 2014 Jelle Alten. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end