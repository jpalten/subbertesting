//
//  ServerConnectionTests.m
//  ServerConnectionTests
//
//  Created by Jelle Alten on 07-02-14.
//  Copyright (c) 2014 Jelle Alten. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OHHTTPStubs.h"
#import "ServerConnection.h"

@interface ServerConnectionTests : XCTestCase
@property(nonatomic, strong) ServerConnection* connection;
@property(nonatomic) BOOL responseReceived;
@end


@interface ServerConnectionTests ()
@end

@implementation ServerConnectionTests

- (void)setUp
{
    NSString* serverUrl = @"https://10.10.10.10/";
    self.connection = [[ServerConnection alloc] initWithUrl:serverUrl];

    self.responseReceived = NO;
}

- (void)tearDown {
    [OHHTTPStubs removeAllStubs];
    [super tearDown];
}


- (void) testWithout_Session
{
    [self stubHttpRequestTo:@"/admin/login" requestBody:@"password=password&user=admin" response:nil];

    NSString* username = @"admin";
    NSString* password = @"password";

    __weak ServerConnectionTests* weakSelf = self;
    SuccessBlock markResponseReceived = ^{
        weakSelf.responseReceived = YES;
    };

    [self.connection nonSessionLogin:username
                            password:password
                             success:markResponseReceived
                             failure:nil
    ];

    [self waitForResponse];
    XCTAssertTrue(self.responseReceived);
}

- (void) testWith_Session
{
    [self stubHttpRequestTo:@"/admin/login" requestBody:@"password=password&user=admin" response:nil];

    NSString* username = @"admin";
    NSString* password = @"password";

    __weak ServerConnectionTests* weakSelf = self;
    SuccessBlock markResponseReceived = ^{
                                 weakSelf.responseReceived = YES;
                             };

    [self.connection sessionLogin:username
                         password:password
                          success:markResponseReceived
                          failure:nil
    ];

    [self waitForResponse];
    XCTAssertTrue(self.responseReceived);
}


- (void)stubHttpRequestTo:(NSString *)expectedUrlPath requestBody:(NSString*)expectedBody response:(NSData *)responseData {

    NSLog(@"preparing response for %@",expectedUrlPath);

    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {

        XCTAssertEqualObjects(request.URL.path, expectedUrlPath);

        NSLog(@"checking body of request %p",request);
        NSString* requestedBody = [[NSString alloc] initWithData:request.HTTPBody encoding:NSUTF8StringEncoding];
        NSLog(@"Would sent body: %@",requestedBody);
        XCTAssertEqualObjects(requestedBody, expectedBody);

        return [request.URL.path isEqualToString:expectedUrlPath];
    } withStubResponse:^OHHTTPStubsResponse *(NSURLRequest *request) {
        NSDictionary *headers = @{@"Content-Type" : @"application/json"};
        return [OHHTTPStubsResponse responseWithData:responseData statusCode:200 headers:headers];
    }];
}



- (void)waitForResponse
{

    NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:5];
    while ([loopUntil timeIntervalSinceNow] > 0 && !self.responseReceived)
    {
        [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                 beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.5]];
    }

}

@end
