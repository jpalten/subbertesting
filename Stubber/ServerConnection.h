//
// Created by jelle on 07-02-14.
//
// 


#import <Foundation/Foundation.h>

@class AFHTTPSessionManager;
@class AFHTTPRequestOperationManager;

typedef void (^SuccessBlock)();
typedef void (^FailureBlock)(NSError* error);


@interface ServerConnection : NSObject

- (id)initWithUrl:(NSString *)urlString;

- (void)nonSessionLogin:(NSString*)username password:(NSString*)password success:(SuccessBlock)success failure:(FailureBlock)failure;

- (void)sessionLogin:(NSString*)username password:(NSString*)password success:(SuccessBlock)success failure:(FailureBlock)failure;

@end