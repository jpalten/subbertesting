//
//  main.m
//  Stubber
//
//  Created by Jelle Alten on 07-02-14.
//  Copyright (c) 2014 Jelle Alten. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }

}